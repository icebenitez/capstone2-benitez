let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e)=>{

	e.preventDefault()

	let name = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value
	let category = document.querySelector("#courseCategory").value
	 
	let token = localStorage.getItem("token")

	// console.log(name)
	// console.log(description)
	// console.log(price)
	// console.log(token)

	fetch('https://fast-sierra-36606.herokuapp.com/api/courses/', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: name,
			price: price,
			description: description,
			category: category
		})
	})
	.then(res => res.json())
	.then(data => {
		if(data){
			window.location.replace("./courses.html")
		} else {
			alert("Something went wrong. Course not added")
		}
	})
	


})
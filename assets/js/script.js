let navItems = document.querySelector("#navSession");

//localStorage > an object used to store information locally in our device
let userToken = localStorage.getItem("token");
let userId = localStorage.getItem("id");
let isAdmin = localStorage.getItem('isAdmin')
//innerHTML = contains all of the element's children as a string.
if (!userToken) {

	console.log(navItems.innerHTML)

	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`
} else {

	if(isAdmin == "true"){

	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log out </a>
		</li>
	`
	} else {

	navItems.innerHTML += 
	`
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log out </a>
		</li>
	

	`
	}
}
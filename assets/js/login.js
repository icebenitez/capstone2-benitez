/*get the form and store in a variable*/
let loginForm = document.querySelector("#logInUser")

/*add a submit event to our login form, so that when the button is clicked to submit, we are able to run a function*/
loginForm.addEventListener("submit", (e) => {

	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	/*console.log(email)
	console.log(password)*/

	//check if our user was able to submit an email or password, if not, we'll show an alert, if yes, we'll proceed to login fetch request.

	if(email == "" || password == ""){

		alert("Please input your email/password.")

	} else {
		fetch('https://fast-sierra-36606.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			//store our token in our localStorage/
			//localStorage is a storage for data in most modern browsers.
			//localStorage, however, can only store strings

			//check if data/response is false or null, if there is no token or false is returned, we will not save it in the localStorage

			if(data !== false && data.accessToken !== null){

				localStorage.setItem('token', data.accessToken)

				fetch('https://fast-sierra-36606.herokuapp.com/api/users/details',{
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem('id', data._id),
					localStorage.setItem('isAdmin', data.isAdmin),
					localStorage.setItem('courses', JSON.stringify(data.enrollments))
					window.location.replace('./courses.html')


					//console.log(data)
				})

			} else {

				alert('Login Failed. Something went wrong.')

			}

		})
	}

})

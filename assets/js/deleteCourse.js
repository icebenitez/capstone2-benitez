let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

let token = localStorage.getItem("token")

let mainText = document.querySelector("#main-text")


//this fetch will run automatically once you get to the page deleteCourse page
fetch(`https://fast-sierra-36606.herokuapp.com/api/courses/deactivate/${courseId}`, {
	method: 'PUT',
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	
	if(data){

		mainText.innerHTML = `<h2>Succesfully Archived</h2>`

	} else {
		alert("Something went wrong.")
	}



})










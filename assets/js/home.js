let navItems = document.querySelector("#navSession");

let greeter = document.querySelector("#userGreeting")

//localStorage > an object used to store information locally in our device
let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin")


if (!userToken) {

	//console.log(navItems.innerHTML)
	
	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
} else {

	if(isAdmin == "true"){

	navItems.innerHTML +=
	`
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log out </a>
		</li>
	`
	} else {

	navItems.innerHTML += 
	`
		<li class="nav-item">
			<a href="./pages/profile.html" class="nav-link"> Profile </a>
		</li>
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log out </a>
		</li>
	

	`
	}
}

if (!userToken){
	console.log(greeter)
	greeter.innerHTML += `Hello, Guest!`
} else {

	if (isAdmin === "true") {
		
		greeter.innerHTML += `Hello, Admin!`

	} else {
		
		greeter.innerHTML += `Hello, User!`

	}
}
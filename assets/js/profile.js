let token = localStorage.getItem('token')
let adminUser = localStorage.getItem('isAdmin')
let profileContainer = document.querySelector("#profileContainer")


fetch(`https://fast-sierra-36606.herokuapp.com/api/users/details`,{
	method: 'GET',
	headers:{
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data)
	let enrollData;

	if(data.isAdmin == true){
		profileContainer.innerHTML = 
		`
			<div class="col-md-12">
				<section class="jumbotron text-center my-5">		
					<h2 class="my-5">No Profile</h2>
				</section>
			</div>
		`

	} else {

		enrollData = data.enrollments.map(course => {
			console.log(course.enrolledOn)
			if(course.length <1){
				return (`<div>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">No Enrollments Yet</h5>
						</div>
					</div>
				</div>`)
			}
			let date = new Date(course.enrolledOn);
			let mm = date.getMonth()+1
			let dd = date.getDate()
			let yy = date.getFullYear()

			return (
				`
				<div>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.courseName}</h5>
							<p class="card-text">Date Enrolled: ${mm}-${dd}-${yy}</p>
							<p class="card-text">Status: ${course.status}</p>
						</div>
					</div>
				</div>

				`
				)

		
		}).join("")

		profileContainer.innerHTML = 
		`
			<div class="col-md-12">
				<section class="jumbotron text-center my-5">		
					<h2 class="my-5">${data.firstName} ${data.lastName}</h2>
					<p>${data.mobileNo}</p>
					${enrollData}
				</section>
			</div>
		`


	}

})






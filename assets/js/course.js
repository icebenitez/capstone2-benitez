//window.location.search -> returns the query string part of the URL


let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

let adminUser = localStorage.getItem('isAdmin')

console.log(courseId)

let token = localStorage.getItem('token')

//console.log(token)

let courseName = document.querySelector('#courseName')
let courseDesc = document.querySelector('#courseDesc')
let coursePrice = document.querySelector('#coursePrice')
let enrollContainer = document.querySelector('#enrollContainer')

fetch(`https://fast-sierra-36606.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data)

	courseName.innerHTML = data.name

	courseDesc.innerHTML = data.description

	coursePrice.innerHTML = data.price

	let enrolleeData;

	if(adminUser === "true"){

		enrollContainer.innerHTML = data.enrollees.map(user => {
			let date = new Date(user.enrolledOn);
			let mm = date.getMonth()+1
			let dd = date.getDate()
			let yy = date.getFullYear()

			return (	
				`
				<div>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${user.userId}</h5>
							<p class="card-text">Date Enrolled: ${mm}-${dd}-${yy}</p>
						</div>
					</div>
				</div>

				`

			)
		}).join("")


	} else {

	if (!token){

		enrollContainer.innerHTML = `<a href="./login.html" class="btn btn-primary text-white btn-block">Enroll</a>`

	} else {	
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

		document.querySelector("#enrollButton").addEventListener("click", ()=>{

		fetch('https://fast-sierra-36606.herokuapp.com/api/users/enroll', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({

				courseId: courseId,
				name: courseName.innerHTML

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				alert("You have enrolled succesfully.")
				window.location.replace("./courses.html")
			} else {
				alert("Enrollment Failed.")
			}


		})
	})
	}
	}
})
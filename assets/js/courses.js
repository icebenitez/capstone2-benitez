let token = localStorage.getItem('token')

//gets a string
let adminUser = localStorage.getItem('isAdmin')

let addButton = document.querySelector('#adminButton')
let cardFooter// will allow us to add a button at the bottom of each of our courses which can redirect to the specific course.
let cardFooterAdmin;

/*if(!token){
	navItems.innerHTML +
	`
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
	`
}*/

if (adminUser === "true"){
	//console.log(addButton)
	addButton.innerHTML += 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class= "btn btn-primary">Add Course</a>
		</div>

	`
	fetch('https://fast-sierra-36606.herokuapp.com/api/courses/all',{
		method: 'GET',
		headers:{
			'Authorization': `Bearer ${token}`}
	})
	.then(res => res.json())
	.then(data => {let courseData = data.map(course => {

		//each item in the array is iterated
		console.log(course)
		cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go to Course</a>`

		if (course.isActive === false){
		cardFooterAdmin = `<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Re-activate Course</a>`
		//^^need to add functionality to activate a course

		} else {
			cardFooterAdmin = `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Course</a>`}
		
		return (
		` 
		<div class="col-md-6 my-3">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">${course.name}</h5>
					<p class="card-text text-left">${course.description}</p>
						<p class="card-text text-right">$${course.price}</p>
				</div>
				<div class="card-footer">
					${cardFooter}
					${cardFooterAdmin}
				</div>
			</div>
		</div>
		`
		)
	}).join("")
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData

})
} else {
	addButton.innerHTML = null

fetch('https://fast-sierra-36606.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	let courseData;

	if(data.length <1){

		courseData = "No Courses Available"

	} else {

		courseData = data.map(course => {

			//each item in the array is iterated
			console.log(course)
			cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go to Course</a>`
			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-right">$${course.price}</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>

				`

				)
		}).join("")
		//join() - joins all of the array elements/items into a string. the argument passed in the method becomes the separator for each item because by default, each item is separated by a comma.

	console.log(courseData)

	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData

}
})
}



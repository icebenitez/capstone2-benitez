let registerForm = document.querySelector("#registerForm");

// .addEventListener("event_name", callbackfunction())
registerForm.addEventListener("submit", (e) => {

	//kapag may mali si user, hindi mawawala mga input
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	console.log(firstName);
	console.log(lastName);
	console.log(mobileNumber);
	console.log(email);
	console.log(password1);
	console.log(password2);

	if((password1 !== '' & password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

	/*Fetch is a built in js function that allows us to get data from another source without the need to refresh the page

	fetch sends the data to the url provided with its parameter:

	fetch(<url>, <parameter>)

	parameters may contain:
	//method -> http method (should reflect the http method defined in you backend)
	//headers
		-> Content-Type - defines what king of data to send
		-> authorization -> contains our bearer-token
	//
	//body -> the body of our request or req.body


	*/

		fetch ('https://fast-sierra-36606.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		//data is the response now in a json format.
		.then(data => {

			//data -> true or false
			//if true, then the email already exists in our database
			//if false, then the email has yet to be registered.
			//this will check if the email exists or not, if the email exists, then will register the user, if not, we're going to show an alert
			if(data === false){
				fetch('https://fast-sierra-36606.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNumber,
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data === true){
						alert("Registration Successful")
	//this method allows to replace the current document witht the document provided in the method which means that for a succesful registration, we will be redirected to our login page
						window.location.replace("./login.html")
					} else {
						alert("Registration Failed")
					}
				})
			} else {

				alert("Email already exists.")

			}
		})
	} 

})